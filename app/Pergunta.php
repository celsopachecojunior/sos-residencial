<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pergunta extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'materia_id', 'pergunta', 'ra','rb','rc','rd','re','rcorreta', 'verificada'   
    ];

    public function materia() 
    {
        return $this->belongsTo('App\Materia'); 
        
    }    

}
