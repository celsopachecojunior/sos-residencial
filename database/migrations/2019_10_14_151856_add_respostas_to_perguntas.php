<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRespostasToPerguntas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perguntas', function (Blueprint $table) {

            $table->text('ra');
            $table->text('rb');
            $table->text('rc');
            $table->text('rd');
            $table->text('re');
            $table->text('rcorreta');   
            $table->text('verificada')->nullable()->comment('Se pergunta foi verificada');  ;   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perguntas', function (Blueprint $table) {
            //
        });
    }
}
