import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

    spinner: {show:false},
    usuarioLogado:'', 
    token : localStorage.getItem('token') || '', 
    

    itensMenu:[
      {texto:"Serviços", routerLinkTo:{name:"servicos"}},
      {texto:"Orçamentos", routerLinkTo:{path:"/orcamento/0"}}, 
      {texto:"Contato", routerLinkTo:{name:"contato"}},
      //{texto:"Contato", name:"contato"},  
      /*
      {texto:"Administrativo", routerLinkTo:{name:"contato"}, subitens:[
        {texto:"Gerenciar Serviços", routerLinkTo:{name:"orcamento"}}, 
      ]},
      */
      /*
      {texto:"Login", routerLinkTo:{name:"login"}},
      */
      ],    

    servicos:[  
      {id:'Alvenaria',nome:"Alvenaria"},	
      {id:'Chuveiros',nome:"Chuveiros"},
      {id:'Descarga',nome:"Descarga"},
      {id:'Desentupimentos',nome:"Desentupimentos"},
      {id:'Elétrica',nome:"Elétrica"},
      {id:'Hidráulica',nome:"Hidráulica"},
      {id:'Interruptores',nome:"Interruptores"},	
      {id:'Lâmpadas',nome:"Lâmpadas"},
      {id:'Pias e ralos',nome:"Pias e ralos"},
      {id:'Pinturas',nome:"Pinturas"},
      {id:'Pisos e azulejos',nome:"Pisos e azulejos"},
      {id:'Portas e maçanetas',nome:"Portas e maçanetas"},
      {id:'Prateleiras',nome:"Prateleiras"},
      {id:'Tomadas',nome:"Tomadas"},
      {id:'Outros Serviços',nome:"Outros Serviços"} 

  ]         

  },
  mutations: { 
		setSpinner : function(state, spinner){ 
			console.log("state.spinner=", spinner)
			state.spinner = spinner;  
    },
		setUsuarioLogado : function(state, usuarioLogado){
			console.log("state.usuarioLogado=", usuarioLogado)
			state.usuarioLogado = usuarioLogado; 
    },
		setToken: function(state, token){
			state.accessToken = token;   
		},    

		setLogout : function(state){
      state.usuarioLogado = '';    
      state.token = "";
      localStorage.removeItem('token'); 
      console.log('store.setLogout concluido'); 

		}          

  },
  actions: {

  }
})
