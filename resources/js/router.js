import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue' 
import Sugestao from './views/Sugestao.vue' 
import Orcamento from './views/Orcamento.vue' 
import Contato from './views/Contato.vue' 
import Servicos from './views/Servicos.vue' 
import LoginSuccessProvider from './views/LoginSuccessProvider.vue'  


 
export default new Router({ 
  routes: [
    { path: '/',      name: 'home',      component: Home }, 
    { path: '/login',      name: 'login',       component: Login },    
    { path: '/login-success-provider',      name: 'login-success-provider',       component: LoginSuccessProvider },
    { path: '/sugestao',      name: 'sugestao',       component: Sugestao },
    { path: '/orcamento/:servico',      name: 'orcamento',       component: Orcamento },
    { path: '/contato',      name: 'contato',       component: Contato },
    { path: '/servicos',      name: 'servicos',       component: Servicos } 
  ], 
  //mode: 'history', 
  scrollBehavior (to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve( savedPosition || { x: 0, y: 0 } )
      }, 1000)
    })
  }
  
})
